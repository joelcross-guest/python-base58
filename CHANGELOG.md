## Changelog

### v0.2.4 / 2016-10-28
- [#8](https://github.com/keis/base58/pull/8) Package metadata (@keis)

### v0.2.3 / 2016-06-14
- [#6](https://github.com/keis/base58/pull/6) Improve error message when the type is not bytes (@keis, @zkanda)

### v0.2.2 / 2015-04-09
- [#3](https://github.com/keis/base58/pull/3) test round trips (@oconnor663)
- [#2](https://github.com/keis/base58/pull/2) fix encoding of empty string (@keis)
